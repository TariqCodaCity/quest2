﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public static class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {
            context.Database.EnsureCreated();
            if(!context.TimeSlots.Any())
            {
                context.AddRange
                    (
                    new TimeSlot { TimeSlotTitle = 1 },
                    new TimeSlot { TimeSlotTitle = 2 },
                    new TimeSlot { TimeSlotTitle = 3 },
                    new TimeSlot { TimeSlotTitle = 4 },
                    new TimeSlot { TimeSlotTitle = 5 },
                    new TimeSlot { TimeSlotTitle = 6 },
                    new TimeSlot { TimeSlotTitle = 7 },
                    new TimeSlot { TimeSlotTitle = 8 },
                    new TimeSlot { TimeSlotTitle = 9 },
                    new TimeSlot { TimeSlotTitle = 10 }
                    );
                context.SaveChanges();
            }
            if (!context.Treatments.Any())
            {
                context.AddRange
                    (
                    new Treatment {TreatmentTitle="first",TreatmentDescription="firstD" },
                    new Treatment { TreatmentTitle = "second", TreatmentDescription = "secondD" },
                    new Treatment { TreatmentTitle = "third", TreatmentDescription = "thirdD" },
                    new Treatment { TreatmentTitle = "4th", TreatmentDescription = "4thD" },
                    new Treatment { TreatmentTitle = "5th", TreatmentDescription = "5thD" },
                    new Treatment { TreatmentTitle = "6th", TreatmentDescription = "6thD" },
                    new Treatment { TreatmentTitle = "7th", TreatmentDescription = "7thD" },
                    new Treatment { TreatmentTitle = "8th", TreatmentDescription = "8thD" },
                    new Treatment { TreatmentTitle = "9th", TreatmentDescription = "9thD" },
                    new Treatment { TreatmentTitle = "10th", TreatmentDescription = "10thD" }
                    );
                context.SaveChanges();
            }
          
            if (!context.Books.Any())
            {
                context.AddRange
                  (
                    new Book {  Name = "Tariq", Mobile = "0798482345", TreatmentId = 1, TimeSlotId = 1, Note = "I want doctor number 1 ", Email = "tariq@coda-city.com", BookedDate = DateTime.Parse("01/02/2018"), IsDeleted = true },
                    new Book { Name = "Aalaa", Mobile = "0790107576", TreatmentId = 2, TimeSlotId = 2, Note = "I want doctor number 2 ", Email = "Aalaa@coda-city.com", BookedDate = DateTime.Parse("05/02/2018"), IsDeleted = true },
                    new Book {  Name = "Seraa", Mobile = "0798482345", TreatmentId = 3, TimeSlotId = 3, Note = "I want doctor number 3 ", Email = "seraa@coda-city.com", BookedDate = DateTime.Parse("10/03/2018"), IsDeleted = true },
                    new Book { Name = "Ahmad", Mobile = "0798485647", TreatmentId = 4, TimeSlotId = 4, Note = "I want doctor number 4 ", Email = "ahmad@coda-city.com", BookedDate = DateTime.Parse("11/11/2018"), IsDeleted = true },
                    new Book { Name = "Muhammad", Mobile = "0744523987", TreatmentId = 5, TimeSlotId = 5, Note = "I want doctor number 5 ", Email = "muhammad@coda-city.com", BookedDate = DateTime.Parse("05/12/2018"), IsDeleted = true },
                    new Book { Name = "Sumaia", Mobile = "0777856954", TreatmentId = 6, TimeSlotId = 6, Note = "I want doctor number 6 ", Email = "sumaia@coda-city.com", BookedDate = DateTime.Parse("07/04/2018"), IsDeleted = true },
                    new Book {  Name = "Hadeel", Mobile = "0784856325", TreatmentId = 7, TimeSlotId = 7, Note = "I want doctor number 7 ", Email = "hadeel@coda-city.com", BookedDate = DateTime.Parse("09/02/2018"), IsDeleted = true },
                    new Book { Name = "Batool", Mobile = "0765123456", TreatmentId = 8, TimeSlotId = 8, Note = "I want doctor number 8 ", Email = "batool@coda-city.com", BookedDate = DateTime.Parse("06/06/2018"), IsDeleted = true },
                    new Book {  Name = "Fatima", Mobile = "0778412578", TreatmentId = 9, TimeSlotId = 9, Note = "I want doctor number 9 ", Email = "fatima@coda-city.com", BookedDate = DateTime.Parse("04/04/2018"), IsDeleted = true },
                    new Book {  Name = "Mahmoud", Mobile = "0788457874", TreatmentId = 10, TimeSlotId = 10, Note = "I want doctor number 10 ", Email = "mahmoud@coda-city.com", BookedDate = DateTime.Parse("03/05/2018"), IsDeleted = true }
                    );

                 context.SaveChanges();
            }
         
        }
    }
}
