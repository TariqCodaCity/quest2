﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public class Book
    {
        [Required, Key]
        public int BookingId { get; set; }
        [Required,StringLength(56, MinimumLength =3,ErrorMessage ="Enter a vilid name")]
        public string Name { get; set; }
        [Required, StringLength(56, MinimumLength =8,ErrorMessage ="Enter a valid mobile number "), DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        [Required,DataType(DataType.Date)]
        public DateTime BookedDate { get; set; }
        [Required,StringLength(56,MinimumLength =5,ErrorMessage ="Enter a valid Email"),DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        [StringLength(1000), DataType(DataType.MultilineText)]
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
        [Required]
        public int TimeSlotId { get; set; }
        [Required]
        public int TreatmentId { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }
        public virtual Treatment Treatment { get; set; }
    }
}
