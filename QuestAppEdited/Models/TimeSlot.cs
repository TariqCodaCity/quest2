﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public class TimeSlot
    {
        [Required, Key]
        public int Id { get; set; }
        public int TimeSlotTitle { get; set; }
        public List<Book> Books { get; set; }
    }
}
