﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QuestAppEdited.Models;
using QuestAppEdited.ViewModels;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuestAppEdited.Controllers
{
    public class HomeController : Controller
    {
        private AppDbContext _appDbContext;
        private readonly IBookRepository _bookRepository;

        public HomeController(IBookRepository bookRepository, AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _bookRepository = bookRepository;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var books = _bookRepository.GetAllBooks().OrderBy(b => b.Name);
            var homeViewModel = new HomeViewModel()
            {
                Title = "Welcome To QuestAppliction (^_^')",
                Books = books.Where(b => b.IsDeleted == false).ToList()

            };
            return View(homeViewModel);
        }

        [HttpPost]
        public IActionResult Index(string searchString)
        {
            var books = _bookRepository.GetAllBooks().Where(b => b.IsDeleted == false).OrderBy(b => b.Name);
            if (searchString != null)
            {

                var homeViewModeSuccess = new HomeViewModel()

                {
                    Title = "Welcome To QuestAppliction (^_^')",
                    Books = books.Where(b => b.Name.Contains(searchString) || b.Mobile.Contains(searchString)).ToList()
                };
                var homeViewModeFail = new HomeViewModel()

                {
                    Title = "Welcome To QuestAppliction (^_^')",
                    Books = books.Where(b => b.Name.Contains(searchString) || b.Mobile.Contains(searchString)).ToList()
                };
                return View(homeViewModeSuccess);
            }
            else
            {
                var homeViewModeFail = new HomeViewModel()

                {
                    Title = "Welcome To QuestAppliction (^_^')",
                    Books = books.ToList()
                };
                return View(homeViewModeFail);
            }
        }

        public IActionResult Details(int id)
        {
            List<Treatment> treatList = new List<Treatment>();
            treatList = _appDbContext.Treatments.ToList();
            ViewBag.ListOfItems = treatList;
            List<TimeSlot> TimeSlotList = new List<TimeSlot>();
            TimeSlotList = _appDbContext.TimeSlots.ToList();
            ViewBag.ListOfTimes = TimeSlotList;
            var book = _bookRepository.GetBookById(id);
            if (book == null)
                return NotFound();
            return View(book);
        }
        [HttpGet]
        public IActionResult DeleteBook(int id)
        {
            var book = _bookRepository.GetBookById(id);
            if (book == null)
                return NotFound();
            return View(book);
        }

        [HttpPost]
        public IActionResult DeleteBook(Book book)
        {
            if (ModelState.IsValid)
            {
                _bookRepository.UpdateBook(book);
                return RedirectToAction("Index");
            }

            return View(book);
        }




        [HttpGet]
        public IActionResult UpdateBook(int id)
        {
            var book = _bookRepository.GetBookById(id);
            if (book == null)
                return NotFound();
            return View(book);
        }
        [HttpPost]
        public IActionResult UpdateBook(Book book)
        {
            if (ModelState.IsValid)
            {
                _bookRepository.UpdateBook(book);
                return RedirectToAction("Index");
            }

            return View(book);
        }



        [HttpGet]
        public IActionResult AddBook() {
              List<Treatment> treatList = new List<Treatment>();
            treatList = _appDbContext.Treatments.ToList();
            ViewBag.ListOfItems = treatList;
            List<TimeSlot> TimeSlotList = new List<TimeSlot>();
            TimeSlotList = _appDbContext.TimeSlots.ToList();
            ViewBag.ListOfTimes = TimeSlotList;

            return View();
        }
       
        [HttpPost]
        public IActionResult AddBook(Book book)
        {
            var IsExisited = _bookRepository.GetBookByEmail(book.Email);
            if(IsExisited == null || IsExisited.IsDeleted == true)
            {
                if (ModelState.IsValid)
                {
                    _bookRepository.AddBook(book);
                    return RedirectToAction("Index");
                }
                return View(book);
            }

            return View(book);
        }
    }
}
